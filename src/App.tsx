import { useState, useEffect } from 'react';

import './App.css';
import { makeItem, Item } from './utils/data';

function App() {
  const [data, setData] = useState<Item[]>([]);

  useEffect(() => {
    const sample = Array(30)
      .fill(0)
      .map((_) => makeItem());

    setData(sample);
  }, []);

  return (
    <div>
      <div className="header">React Starter</div>
      <div className="topnav">
        <a href="#">some option</a><a href="#">another option</a>
      </div>
      <div>
        <div className="column side">&nbsp;</div>
        <div className="column middle">
          <h2>Feed of Content</h2>
          <ul id="data-list">
            {data.map((item, idx) => {
              return (
                <li key={idx}>
                  {`${item.createdAt.toDateString()} - ${item.name} - ${
                    item.detail
                  }`}
                </li>
              );
            })}
          </ul>
        </div>
      </div>
      <div className="footer">{`© ${new Date().getFullYear()}`}</div>
    </div>
  );
}

export default App;
