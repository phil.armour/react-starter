export interface Item {
  name: string;
  createdAt: Date;
  detail: string;
  deleted: boolean;
}

function getRandomInt(max: number) {
  return Math.floor(Math.random() * Math.floor(max));
}

const names = [
  'Carey Mcpherson',
  'Lorelei Snellgrove',
  'Slyvia Vine',
  'Ivonne Weyand',
  'Kacy Lebow',
  'Tanya Yowell',
  'Valentin Nida',
  'Jackeline Gertz',
  'Ryann Riegel',
  'Ha Mellor',
  'Terica Hoffert',
  'Loura Keleher',
  'Dann Minks',
  'Dewitt Sands',
  'Sam Clevenger',
  'Anja Mayweather',
  'Marla Vassel',
  'Anh Heppner',
  'Amanda Fonseca',
  'Laverne Stober',
];

const details = [
  'Another journey chamber way yet females man.',
  'Way extensive and dejection get delivered deficient sincerity gentleman age.',
  'Too end instrument possession contrasted motionless.',
  'Calling offence six joy feeling.',
  'Coming merits and was talent enough far.',
  'Sir joy northward sportsmen education.',
  'Discovery incommode earnestly no he commanded if.',
  'Put still any about manor heard.',
  'Village did removed enjoyed explain nor ham saw calling talking.',
  'Securing as informed declared or margaret.',
  'Joy horrible moreover man feelings own shy.',
  'Request norland neither mistake for yet.',
  'Between the for morning assured country believe.',
  'On even feet time have an no at.',
  'Relation so in confined smallest children unpacked delicate.',
  'Why sir end believe uncivil respect.',
  'Always get adieus nature day course for common.',
  'My little garret repair to desire he esteem.',
  'Article nor prepare chicken you him now.',
  'Shy merits say advice ten before lovers innate add.',
];

export const makeItem = (): Item => {
  const name = names[getRandomInt(20)];
  const createdAt = new Date(2020, getRandomInt(12), getRandomInt(28));
  const detail = details[getRandomInt(20)];
  const deleted = getRandomInt(2) === 0 ? true : false;
  return { name, createdAt, detail, deleted };
};

function timeout(ms: number) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

export const getData = async (page: 1|2): Promise<string[]> => {
  await timeout(100);
  return page === 1 ? details.slice(0,10) : details.slice(10,20)
}
