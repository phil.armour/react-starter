import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';

test('renders React Starter header', () => {
  render(<App />);
  const headerElement = screen.getByText(/React Starter/i);
  expect(headerElement).toBeInTheDocument();
});

test('renders Feed of Content title', () => {
  render(<App />);
  const headerElement = screen.getByText(/Feed of Content/i);
  expect(headerElement).toBeInTheDocument();
});
